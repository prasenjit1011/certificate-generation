<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class Trainee extends Model
{
    use SoftDeletes;

    public $table = 'trainees';

    const EMPLOYEE_TYPE_SELECT = [
        '1' => 'Internal Employee',
        '2' => 'External Employee',
    ];

    protected $dates = [
        'issue_date',
        'certificate_approve_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const CERTIFICATE_APPROVAL_STATUS_SELECT = [
        '1' => 'Pending',
        '2' => 'Approved',
        '3' => 'Canceled',
    ];

    const EMP_CATEGORY_SELECT = [
        '1' => 'Mr.',
        '2' => 'Mrs.',
        '3' => 'Ms.',
        '4' => 'Dr.',
        '5' => 'Eng.',
        '6' => 'Others',
    ];

    protected $fillable = [
        'emp_category',
        'first_name',
        'last_name',
        'institution_name',
        'emailid',
        'contact_no',
        'issue_date',
        'serial_no',
        'certificate_approval_status',
        'certificate_approve_by',
        'certificate_approve_date',
        'certificate_qrcode',
        'default_certificate',
        'created_by',
        'employee_type',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function getIssueDateAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setIssueDateAttribute($value)
    {
        $this->attributes['issue_date'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }

    public function getCertificateApproveDateAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setCertificateApproveDateAttribute($value)
    {
        $this->attributes['certificate_approve_date'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }
}
