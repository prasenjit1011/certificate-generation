<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CloudHostingProduct extends Mailable
{
    use Queueable, SerializesModels;
    public $name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name)
    {
        $this->name = $name;
    }
    public function build()
    {
        return $this->view('admin.employees.mailtemplate');
    }

    /**
     * Build the message.
     *
     * @return $this
     
    public function build()
    {
		return $this->from('cloudways@cloudways.com')->view('emails.CloudHosting.Product');
        return $this->view('view.name');
    }*/
}
