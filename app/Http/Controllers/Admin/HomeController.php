<?php

namespace App\Http\Controllers\Admin;
use App\Employee;
use Auth;

class HomeController
{
    public function index()
    {
		$uid = Auth::user()->id;
		$page = 'dashboard';
		if(Auth::user()->roles[0]->title == 'Admin'){
			$data['totalemployee'] = Employee::get()->count();
			$data['totalpending'] 	= Employee::where('certificate_approval_status',1)->get()->count();
			$data['totalreviewed'] 	= Employee::where('certificate_approval_status',4)->get()->count();
			$data['totalapproved'] 	= Employee::where('certificate_approval_status',2)->get()->count();			
		}
		else{
			$data['totalemployee'] = Employee::where('created_by',$uid)->get()->count();
			$data['totalpending'] 	= Employee::where('created_by',$uid)->where('certificate_approval_status',1)->get()->count();
			$data['totalreviewed'] 	= Employee::where('created_by',$uid)->where('certificate_approval_status',4)->get()->count();
			$data['totalapproved'] 	= Employee::where('created_by',$uid)->where('certificate_approval_status',2)->get()->count();			
		}
		//dd($data);
        return view('home',compact('data','page'));
    }
}
