<?php
namespace App\Http\Controllers\Admin;
use App\Certificate;
use App\Employee;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Requests\MassDestroyEmployeeRequest;
use App\Http\Requests\StoreEmployeeRequest;
use App\Http\Requests\UpdateEmployeeRequest;
use App\Internalmember;
use App\Department;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use PDF;
use Auth;
use DB;
use URL;
use Route;
//use Mail;
use Libern\QRCodeReader\QRCodeReader;
use Zxing\QrReader;
use App\Mail\MailtrapExample;
use Illuminate\Support\Facades\Mail;


class EmployeeController extends Controller
{
    use CsvImportTrait;

	function sendmail(Request $request) {
		
		$res = Employee::where('id',$request->id)->get()->toArray();
		if(isset($res[0]['pdf_generated']) && $res[0]['pdf_generated'] == 0){
			$this->certificatepdf($request->id);
		}
		//$this->certificatepdf($request->id);
		
		
		//dd(public_path('/'));
		$data = [
			"to" => "prasenjit.aluni@gmail.com",
			"attachments" => [
				[
					"path" => 'pdf/certificate-'.$request->id.'.pdf',
					"as" => "certificate.pdf",
					"mime" => "application/pdf",
				]
			],
		];		
		Mail::to($data['to'])->send(new MailtrapExample($data));
		Employee::where('id',$request->id)->update(array('mail_sent'=>1));
		return redirect()->route('admin.employees.index')->with('success','Mail sent successfully');;;;
		return 'Mail sent successfully!';
		
		
		
		$data = array();
		$pathToFile = 'pdf\certificate-20.pdf';
		Mail::to('prasenjit.aluni@gmail.com')->send(new MailtrapExample(), $data, function ($message) {
			$pathToFile = asset('pdf/certificate-20.pdf');
			$message->attach('images/sitelogo.jpg', ['as' => 'Logo', 'mine'=>'application/jpeg']);
		});
		//Mail::to('prasenjit.aluni@gmail.com')->send(new MailtrapExample(), $data, function ($message) {$message->attach('pdf/certificate-20.pdf');}); 
		
	}

	
	public function democode(){}
	
	public function allmember(Request $request){
		abort_if(Gate::denies('employee_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		$certificate = array();
		$res = Certificate::select('id','certificate_title','department')->get()->toArray();
		
		foreach($res as $val){
			$certificate[$val['id']]['id'] = $val['id'];
			$certificate[$val['id']]['certificate_title'] 	= $val['certificate_title'];
			$certificate[$val['id']]['department'] 			= $val['department'];
		}
		//dd($certificate);
        $employees = Employee::get();
		
		if(Auth::user()->roles[0]->title == 'Admin'){
			$employees = Employee::get();
		}
		elseif(Auth::user()->roles[0]->title == 'Officer'){
			$employees = Employee::where('created_by',Auth::user()->id)->get();
		}
		else{			
			$employees = Employee::where('department',Auth::user()->department)->get();
		}
		
		
        $users = User::all();
		$usersname = array();
		foreach($users as $val){
			$usersname[$val->id] = $val->name;
		}
		$type = 3;
		$mass_actiontxt = '';
		if(Auth::user()->roles[0]->title == 'Manager'){
			$mass_actiontxt = 'Reviewed';
		}
		elseif(Auth::user()->roles[0]->title == 'HOD'){
			$mass_actiontxt = 'Approved';
		}
        return view('admin.employees.external.index', compact('employees','certificate','usersname','type','mass_actiontxt'));
	}
	
	
    public function index(Request $request){
		//dd(attachFromStorage());
		//dd(Route::current()->getName(),URL::current());

		
        abort_if(Gate::denies('employee_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		$certapprovestatus = 1;
		
		if(in_array(Auth::user()->roles[0]->title,array('TraineeOfficer','StrategyOfficer'))){
			$certapprovestatus = 2;
			$uar['certificate_approval_status'] = 2;
			//Employee::whereIn('department',['TraningDept','StrategyDept'])->update($uar);			
		}		
		/*
		$type = 'external';
		if($request->type == 'internal'){
			$type = 'internal';
		}
		*/
				$certificate = array();
		$res = Certificate::select('id','certificate_title','department')->get()->toArray();
		
		foreach($res as $val){
			$certificate[$val['id']]['id'] = $val['id'];
			$certificate[$val['id']]['certificate_title'] 	= $val['certificate_title'];
			$certificate[$val['id']]['department'] 			= $val['department'];
		}
		//dd($certificate);
		
		$employees = array();
		$whr = array();
        $whereIn = array();
		$whr['employee_type'] = 2;
		if(Auth::user()->roles[0]->title == 'Admin' || Auth::user()->roles[0]->title == 'Principal'){
			if(Route::current()->getName() == 'admin.employees.stategy.listing'){
				$whr['emp_type']	= 2;				
				$employees = Employee::where($whr)->get();				
			}			
			elseif(Route::current()->getName() == 'admin.employees.trainee.listing'){
				$whr['emp_type']	= 3;				
				$employees = Employee::where($whr)->get();
			}			
			else{
				$whr['emp_type']	= 1;
				//$employees = Employee::where($whr)->whereNotIn('department',array('StrategyDept','TraningDept'))->get();
				$employees = Employee::where($whr)->get();
			}
			
		}
		elseif(Auth::user()->roles[0]->title == 'Officer'){
			$whr['emp_type'] 		= Auth::user()->user_type;
			$whr['created_by']		= Auth::user()->id;
			//$whr['certificate_approval_status']	= 1;
			$employees = Employee::where($whr)->get();
			//$employees = Employee::where('employee_type',2)->where('created_by',Auth::user()->id)->get();
		}
		elseif(Auth::user()->roles[0]->title == 'Manager'){
			$whr['emp_type'] 	= Auth::user()->user_type;
			$whr['department']	= Auth::user()->department;
			$employees = Employee::where($whr)->get();
			//$employees = Employee::where($whr)->whereIn('certificate_approval_status',[1,4])->get();
		}
		elseif(Auth::user()->roles[0]->title == 'HOD'){
			$whr['emp_type'] 		= Auth::user()->user_type;
			$whr['employee_type'] 	= 2;
			$whr['department']		= Auth::user()->department;
			//$whr['certificate_approval_status']	= 4;
			//$whr = array();
			$employees = Employee::where($whr)->get();
		}
		
        $users = User::all();
		$usersname = array();
		foreach($users as $val){
			$usersname[$val->id] = $val->name;
		}
		$type = 2;
		$mass_actiontxt = '';
		if(Auth::user()->roles[0]->title == 'Manager'){
			$mass_actiontxt = 'Reviewed';
		}
		elseif(Auth::user()->roles[0]->title == 'HOD'){
			$mass_actiontxt = 'Approved';
		}
		elseif(in_array(Auth::user()->roles[0]->title,array('TraineeOfficer','StrategyOfficer'))){
			$mass_actiontxt = 'Approved';
		}
		//dd($employees);
		
        return view('admin.employees.external.index', compact('employees','certificate','usersname','type','mass_actiontxt'));

    }

    public function create(){
        abort_if(Gate::denies('employee_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		$certapprovestatus = 1;
		
		if(in_array(Auth::user()->roles[0]->title,array('TraineeOfficer','StrategyOfficer'))){
			$certapprovestatus = 2;
			$uar['certificate_approval_status'] = 2;
			//Certificate::where('department','StrategyDept')->update($uar);			
		}		
		$department = Department::get();
		
		$certificate = array();
		//dd(Auth::user()->roles[0]->title);
		//dd(Auth::user()->department);
		if(Auth::user()->roles[0]->title == 'Admin' || Auth::user()->roles[0]->title == 'Principal'){
			$res = Certificate::select('id','certificate_title','department','serial_prefix','serial_no')->get()->toArray();
		}
		else{
			$res = Certificate::select('id','certificate_title','department','serial_prefix','serial_no')->where('type',Auth::user()->user_type)->where('department',Auth::user()->department)->get()->toArray();
		}
		
		
		foreach($res as $val){
			$certificate[$val['id']]['id'] 					= $val['id'];
			$certificate[$val['id']]['certificate_title'] 	= $val['certificate_title'];
			$certificate[$val['id']]['department'] 			= $val['department'];
			$certificate[$val['id']]['serial_prefix'] 		= $val['serial_prefix'];
			$certificate[$val['id']]['serial_no'] 			= $val['serial_no']+1;
			$serial_no 										= $val['serial_no']+1;			
		}
		if(count($certificate) == 0){
			echo "<h3>Please create certificate.</h3>";
			exit;
		}
		
		$type = 2;

		//dd($serial_no);
		//serial_prefix,serial_no
		
        return view('admin.employees.external.create', compact('certificate','type','certapprovestatus','department','serial_no'));
    }

    public function store(StoreEmployeeRequest $request)
    {
		/*if(in_array(Auth::user()->roles[0]->title,array('TraineeOfficer','StrategyOfficer'))){
			$request->certificate_approval_status = 2;
		}
		if(Auth::user()->roles[0]->title == 'HOD'){
			$request->certificate_approval_status = 4;
		}*/
		//$request->emp_type = Auth::user()->user_type;
		
		if(!empty($request->serial_no)){
			Certificate::where('id',$request->default_certificate)->update(array('serial_no'=>$request->serial_no));
		}
		
		//dd($request->default_certificate, $request->serial_no);
		
		
		
		$employee = Employee::create($request->all());
		//dd(Auth::user()->roles[0]->title,$request->all());
		
		if($request->employee_type == 1){
			return redirect()->route('admin.members.index');
		}
		else{
			return redirect()->route('admin.employees.index');
		}		        
    }

    public function edit(Employee $employee)
    {
        abort_if(Gate::denies('employee_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		$certificate = array();
		$res = Certificate::select('id','certificate_title','department')->get()->toArray();
		
		foreach($res as $val){
			$certificate[$val['id']]['id'] = $val['id'];
			$certificate[$val['id']]['certificate_title'] = $val['certificate_title'];
			$certificate[$val['id']]['department'] 			= $val['department'];
		}
		$department = Department::get();
        return view('admin.employees.external.edit', compact('employee','certificate','department'));
    }

    public function update(UpdateEmployeeRequest $request, Employee $employee)
    {
        $employee->update($request->all());
		if($request->employee_type == 1){
			return redirect()->route('admin.members.index');
		}
		else{
			return redirect()->route('admin.employees.index');
		}        
    }

    public function show(Employee $employee)
    {
        abort_if(Gate::denies('employee_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.employees.external.show', compact('employee'));
    }

    public function destroy(Employee $employee)
    {
        abort_if(Gate::denies('employee_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $employee->delete();

        return back();
    }

    public function massDestroy(MassDestroyEmployeeRequest $request)
    {
		//print_r($request->ids);
        
		
		if(Auth::user()->roles[0]->title == 'HOD' || Auth::user()->roles[0]->title == 'Principal'){
			$updatedata = array('certificate_approve_by'=>Auth::user()->id,'certificate_approval_status'=>2);
			//Employee::whereIn('id', request('ids'))->where('certificate_approval_status',4)->update($updatedata);
			Employee::whereIn('id', request('ids'))->whereIn('certificate_approval_status',array(1,4))->update($updatedata);
		}
		elseif(Auth::user()->roles[0]->title == 'Manager'){			
			$updatedata = array('reviewed_by'=>Auth::user()->id,'certificate_approval_status'=>4);
			Employee::whereIn('id', request('ids'))->where('certificate_approval_status',1)->update($updatedata);
		}
		//print_r($updatedata);
		
		//exit;
		//Employee::whereIn('id', request('ids'))->delete();
        return response(null, Response::HTTP_NO_CONTENT);
    }
	
	public function certificateapprove(Request $request){		
		if(Auth::user()->roles[0]->title == 'HOD' || Auth::user()->roles[0]->title == 'Principal'){
			$empid = $request->empid;
			$employee 	= Employee::where('id',$empid)->get();
			
			/*if(isset($employee[0]->certificate_approval_status) && $employee[0]->certificate_approval_status == 4){	
			}*/
			Employee::where('id',$empid)->update(array('certificate_approval_status'=>2,'pdf_generated'=>1,'certificate_approve_by'=>Auth::user()->id));
			$this->certificatepdf($empid);												
			if($employee[0]->employee_type == 2){
				return redirect('admin/employees');
			}
			else{
				return redirect('admin/members');
			}						
		}		
		return redirect('admin/members');
	}
	
	public function certificatereview(Request $request){
		//dd(88);
		if(Auth::user()->roles[0]->title == 'Manager'){
			$empid 		= $request->empid;
			$employee 	= Employee::where('id',$empid)->get();
			if(isset($employee[0]->certificate_approval_status) && $employee[0]->certificate_approval_status == 1){
				$empid = $request->empid;
				Employee::where('id',$empid)->update(array('certificate_approval_status'=>4,'reviewed_by'=>Auth::user()->id));
				if($employee[0]->employee_type == 2){
					return redirect('admin/employees');
				}
				else{
					return redirect('admin/members');
				}					
			}
		}		
		return redirect('admin/members');
	}
	
	//public function certificatepdf(Request $request){		
	public function certificatepdf($empid){	

		//dd(Route::current()->getName());
	
		$user			= array();
		//$empid 			= $request->empid;
		$employee 		= Employee::where('id',$empid)->get()->toArray();		
		$certificate 	= Certificate::where('id',$employee[0]['default_certificate'])->get()->toArray();		
		$res 			= User::get()->toArray();
		foreach($res as $val){
			$user[$val['id']] = $val;
		}
		if($employee[0]['emp_id'] == ''){
			$employee[0]['emp_id'] = 0;
		}
		foreach(Employee::EMP_CATEGORY_SELECT as $key => $label){
			if($key == $employee[0]['emp_category']){
				$employee[0]['emp_category'] = $label;
			}
		}


		
		$qrstr  = "";
		//$qrstr  = $certificate[0]['certificate_title'];
		$qrstr .= 'Certificate Id - '.$employee[0]['id'];
		if($employee[0]['employee_type'] == 1){
			//$qrstr .= '-Internal';
		}
		else{
			//$qrstr .= '-External';
		}
		//$qrstr  = '-'.$employee[0]['first_name'];
		//$qrstr .= '-'.$employee[0]['last_name'];
		//$qrstr .= '-'.$employee[0]['department'];
		if($employee[0]['issue_date'] != NULL){
			$qrstr .= $employee[0]['issue_date'];
		}		
		if($employee[0]['issue_date'] != NULL){
			$qrstr .= $employee[0]['serial_no'];
		}
		
		//$qrstr = "test";
		//dd($certificate);
		
		
		//dd($employee);
		$qrcontent = 'سعيد عبدالله';
		$qrcontent = 'إصدار شهادة متدرب لروهيت شارما بتاريخ 31/07/2020';
		
		$qrcontent = ' اسم : '.$employee[0]['first_name'].' '.$employee[0]['last_name'].', تاريخ الاصدار : '.$employee[0]['issue_date'].', Serial No : '.$employee[0]['serial_no'];
		$qrcontent = iconv ('latin1' , 'utf-8' , $qrcontent);
		//echo $qrcontent;exit;
		//$qrcontent = 'Trainee Certificate issue to Rohit Sharma at '.date('d-m-Y ');
		
		$qrstr = time();
		
		//echo '----'.$_ENV['DB_CONNECTION'];;exit;
		//183,133,55
		
		
		\QrCode::size(500)->format('png')
		->color($_ENV['QRCOLOR1'],$_ENV['QRCOLOR2'],$_ENV['QRCOLOR3'])
		->merge(public_path('images/laravel.jpg'), 0.2, true)
		->size(800)
		->errorCorrection('H')
		->generate($qrcontent, public_path('images/qrcode/qrcode-'.$qrstr.'.png'));		
		
		
		
		$data = [
			'employee' 		=> $employee[0],
			'certificate' 	=> $certificate[0],
			'user' 			=> $user,
			'qrstr'			=> $qrstr
		];
		
		

		
		if(1){
			if($data['certificate']['template'] == 1){			
				$customPaper = array(0,0,1714.00,2464.00);			
				$pdf = PDF::loadView('admin/employees/strategyPDF', $data, ['mode' => 'utf-8','orientation' => 'A4-L'])
				->setPaper($customPaper, 'landscape');//->image('images\certificate_logo.png',10,10,-300,300);;		
			}
			else if($data['certificate']['template'] == 2){
				$customPaper = array(0,0,595.00,842.00);
				$pdf = PDF::loadView('admin/employees/traineePDF', $data, ['mode' => 'utf-8','orientation' => 'A4-L'])->setPaper($customPaper, 'landscape');
				
			}

		}
		else{					
			if($data['certificate']['template'] == 1){			
				$customPaper = array(0,0,1714.00,2464.00);			
				$pdf = PDF::loadView('admin/employees/strategyPDF', $data, ['mode' => 'utf-8','orientation' => 'A4-L'])
				->setPaper($customPaper, 'landscape');//->image('images\certificate_logo.png',10,10,-300,300);;		
			}
			else if($data['certificate']['template'] == 2){
				$customPaper = array(0,0,1244.00,1755.00);
				$pdf = PDF::loadView('admin/employees/traineePDF', $data, ['mode' => 'utf-8','orientation' => 'A4-L'])->setPaper($customPaper, 'landscape');
				
			}
		}
		
		
		
		
		$str = 'certificate-'.$empid.'.pdf';				
		//dd($employee[0]['certificate_approve_by']);
		if($employee[0]['certificate_approval_status'] == 2){
			Employee::where('id',$empid)->update(array('pdf_generated'=>1));
			$pdf = $pdf->save('pdf/'.$str);
		}
		
		if(Route::current()->getName() == 'admin.employees.certificatepdf'){
			return $pdf->stream($str);
		}
	}
	
	public function listing(){
		abort_if(Gate::denies('employee_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		$certificate = array();
		$res = Certificate::select('id','certificate_title','department')->get()->toArray();
		
		foreach($res as $val){
			$certificate[$val['id']]['id'] = $val['id'];
			$certificate[$val['id']]['certificate_title'] 	= $val['certificate_title'];
			$certificate[$val['id']]['department'] 			= $val['department'];
		}
		//dd($certificate);
        $employees = Employee::get();
		
		if(Auth::user()->roles[0]->title == 'Admin'){
			$employees = Employee::get();
		}
		elseif(Auth::user()->roles[0]->title == 'Officer'){
			$employees = Employee::where('created_by',Auth::user()->id)->get();
		}
		else{			
			$employees = Employee::where('department',Auth::user()->department)->get();
		}
		
		
        $users = User::all();
		$usersname = array();
		foreach($users as $val){
			$usersname[$val->id] = $val->name;
		}
		$type = 4;
		$mass_actiontxt = '';
		if(Auth::user()->roles[0]->title == 'Manager'){
			$mass_actiontxt = 'Reviewed';
		}
		elseif(Auth::user()->roles[0]->title == 'HOD'){
			$mass_actiontxt = 'Approved';
		}
        return view('admin.employees.external.index', compact('employees','certificate','usersname','type','mass_actiontxt'));
	}
	
	public function addcertificate(Request $request){
		$id 			= $request->id;
		$certificate	= $request->default_certificate;
		
		$member 		= Internalmember::where('id',$id)->get()->toArray();
		if(isset($member[0])){
			$data["default_certificate"]	= $certificate;
			$data["emp_id"] 			= $member[0]["emp_id"];
			$data["emp_category"] 		= $member[0]["emp_category"];
			$data["first_name"] 		= $member[0]["first_name"];
			$data["last_name"] 			= $member[0]["last_name"];
			$data["institution_name"] 	= $member[0]["institution_name"];
			$data["emailid"] 			= $member[0]["emailid"];
			$data["department"] 		= $member[0]["department"];
			$data["issue_date"] 		= $request->issue_date;
			$data["serial_no"] 			= $request->serial_no;;
			$data["created_by"] 		= Auth::user()->id;
			Employee::insert($data);
		}
		return redirect('/admin/members')->with('success','Added successfully');;;
	}

}
