<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use \DateTimeInterface;

class Certificate extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'certificates';

    protected $appends = [
        'certificate_logo',
        'certificate_signature',
		'stamp'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];


    const TEMPLATE_RADIO = [
        '1' => 'Template 1',
        '2' => 'Template 2',
    ];	
	
    protected $fillable = [
        'template',
		'certificate_issue_by_post',
		'certificate_issue_by_officername',
		'certificate_issue_msg',
		'certificate_title',
        'certificate_subtitle',
        'certificate_details',
		'department',
        'created_by',
        'empid',
		'serial_prefix',
		'serial_no',		
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getCertificateLogoAttribute()
    {
        $file = $this->getMedia('certificate_logo')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
        }

        return $file;
    }
	
    public function getCertificateSignatureAttribute()
    {
        $file = $this->getMedia('certificate_signature')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
        }

        return $file;
    }
	
    public function getStampAttribute()
    {
        $file = $this->getMedia('stamp')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
        }

        return $file;
    }	
}
