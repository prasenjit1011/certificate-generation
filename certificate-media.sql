-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 26, 2020 at 04:42 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newcertificatedevopment`
--

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `manipulations` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `model_type`, `model_id`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `size`, `manipulations`, `custom_properties`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
(1, 'App\\Certificate', 1, 'certificate_logo', '5ee6e584d78e9_united-arab-emirates-university-squarelogo', '5ee6e584d78e9_united-arab-emirates-university-squarelogo.png', 'image/png', 'public', 51211, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 1, '2020-06-14 21:35:43', '2020-06-14 21:35:44'),
(2, 'App\\User', 2, 'signature', '5ee6ed6b3d9a4_signature', '5ee6ed6b3d9a4_signature.png', 'image/png', 'public', 1543, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 2, '2020-06-14 22:09:26', '2020-06-14 22:09:26'),
(4, 'App\\Certificate', 2, 'certificate_logo', '5eecd5dd985e7_ulogo', '5eecd5dd985e7_ulogo.jpg', 'image/jpeg', 'public', 31371, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 3, '2020-06-19 09:42:50', '2020-06-19 09:42:51'),
(5, 'App\\Certificate', 2, 'certificate_logo', '5eecd6003ea50_profile', '5eecd6003ea50_profile.png', 'image/png', 'public', 10350, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 4, '2020-06-19 09:43:25', '2020-06-19 09:43:26'),
(6, 'App\\Certificate', 8, 'certificate_logo', '5eed79da6a549_36397354_1044702075693358_2807173728264257536_n (1)', '5eed79da6a549_36397354_1044702075693358_2807173728264257536_n-(1).jpg', 'image/jpeg', 'public', 161371, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 5, '2020-06-19 21:23:17', '2020-06-19 21:23:28'),
(7, 'App\\Certificate', 8, 'certificate_logo', '5eed7a589531e_36466278_1044699755693590_3334984831955107840_n', '5eed7a589531e_36466278_1044699755693590_3334984831955107840_n.jpg', 'image/jpeg', 'public', 91124, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 6, '2020-06-19 21:24:18', '2020-06-19 21:24:19'),
(8, 'App\\Certificate', 8, 'certificate_logo', '5eed7a8100dac_36426773_1044701452360087_8121610675144359936_n', '5eed7a8100dac_36426773_1044701452360087_8121610675144359936_n.jpg', 'image/jpeg', 'public', 79744, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 7, '2020-06-19 21:24:58', '2020-06-19 21:24:59'),
(9, 'App\\Certificate', 8, 'certificate_logo', '5eed7ad7b1b5e_36426773_1044701452360087_8121610675144359936_n', '5eed7ad7b1b5e_36426773_1044701452360087_8121610675144359936_n.jpg', 'image/jpeg', 'public', 79744, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 8, '2020-06-19 21:26:25', '2020-06-19 21:26:26'),
(10, 'App\\Certificate', 8, 'certificate_logo', '5eed7be2d91ab_36426773_1044701452360087_8121610675144359936_n', '5eed7be2d91ab_36426773_1044701452360087_8121610675144359936_n.jpg', 'image/jpeg', 'public', 79744, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 9, '2020-06-19 21:30:52', '2020-06-19 21:30:53'),
(11, 'App\\Certificate', 8, 'certificate_logo', '5eed82973f8e7_36426773_1044701452360087_8121610675144359936_n', '5eed82973f8e7_36426773_1044701452360087_8121610675144359936_n.jpg', 'image/jpeg', 'public', 79744, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 10, '2020-06-19 21:59:30', '2020-06-19 21:59:31'),
(12, 'App\\Certificate', 8, 'certificate_logo', '5eed82b21f85f_35521614_1032592953570937_1321816197259329536_n', '5eed82b21f85f_35521614_1032592953570937_1321816197259329536_n.jpg', 'image/jpeg', 'public', 333252, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 11, '2020-06-19 21:59:55', '2020-06-19 21:59:56'),
(13, 'App\\Certificate', 8, 'certificate_signature', '5ef58d7f32b0c_logo-icon', '5ef58d7f32b0c_logo-icon.png', 'image/png', 'public', 930, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 12, '2020-06-26 00:24:09', '2020-06-26 00:24:20'),
(14, 'App\\Certificate', 9, 'certificate_logo', '5ef58e6bcf47a_chair', '5ef58e6bcf47a_chair.jpg', 'image/jpeg', 'public', 31299, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 13, '2020-06-26 00:28:09', '2020-06-26 00:28:10'),
(16, 'App\\Certificate', 9, 'certificate_logo', '5ef58e8c8d061_chair4', '5ef58e8c8d061_chair4.jpg', 'image/jpeg', 'public', 28041, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 15, '2020-06-26 00:28:44', '2020-06-26 00:28:45'),
(17, 'App\\Certificate', 1, 'certificate_logo', '5ef58fb04383d_certificate_logo', '5ef58fb04383d_certificate_logo.png', 'image/png', 'public', 44575, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 16, '2020-06-26 00:34:21', '2020-06-26 00:34:21'),
(18, 'App\\Certificate', 1, 'certificate_signature', '5ef58fe435686_signature', '5ef58fe435686_signature.jpeg', 'image/jpeg', 'public', 52973, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 17, '2020-06-26 00:34:21', '2020-06-26 00:34:22'),
(19, 'App\\Certificate', 10, 'certificate_logo', '5ef6c9be91245_DSC02256', '5ef6c9be91245_DSC02256.JPG', 'image/jpeg', 'public', 3714015, '[]', '[]', '[]', 18, '2020-06-26 22:53:49', '2020-06-26 22:53:49'),
(20, 'App\\Certificate', 10, 'certificate_logo', '5ef6ca78564ab_download (1)', '5ef6ca78564ab_download-(1).jpg', 'image/jpeg', 'public', 9956, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 19, '2020-06-26 22:56:39', '2020-06-26 22:56:43'),
(21, 'App\\Certificate', 11, 'certificate_logo', '5ef6cb1480a0b_download', '5ef6cb1480a0b_download.jpg', 'image/jpeg', 'public', 10995, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 20, '2020-06-26 22:59:16', '2020-06-26 22:59:17'),
(22, 'App\\Certificate', 11, 'certificate_signature', '5ef6cb1adade6_green-trees-leafs-jpg-168114194', '5ef6cb1adade6_green-trees-leafs-jpg-168114194.jpg', 'image/jpeg', 'public', 49191, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 21, '2020-06-26 22:59:17', '2020-06-26 22:59:17'),
(23, 'App\\Certificate', 12, 'certificate_logo', '5ef6cbef21f32_download (1)', '5ef6cbef21f32_download-(1).jpg', 'image/jpeg', 'public', 9956, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 22, '2020-06-26 23:02:54', '2020-06-26 23:02:54'),
(24, 'App\\Certificate', 12, 'certificate_signature', '5ef6cbf4b93b4_green-trees-leafs-jpg-168114194', '5ef6cbf4b93b4_green-trees-leafs-jpg-168114194.jpg', 'image/jpeg', 'public', 49191, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 23, '2020-06-26 23:02:54', '2020-06-26 23:02:55'),
(25, 'App\\Certificate', 12, 'stamp', '5ef6d3764bcc0_download', '5ef6d3764bcc0_download.jpg', 'image/jpeg', 'public', 10995, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 24, '2020-06-26 23:34:55', '2020-06-26 23:34:55'),
(26, 'App\\Certificate', 13, 'certificate_logo', '5f0923a1f1662_1', '5f0923a1f1662_1.png', 'image/png', 'public', 2637, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 25, '2020-07-10 20:58:31', '2020-07-10 20:58:42'),
(27, 'App\\Certificate', 13, 'certificate_signature', '5f0923aa15f51_4', '5f0923aa15f51_4.png', 'image/png', 'public', 6047, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 26, '2020-07-10 20:58:42', '2020-07-10 20:58:43'),
(28, 'App\\Certificate', 13, 'stamp', '5f0923a65d7bf_2', '5f0923a65d7bf_2.png', 'image/png', 'public', 14926, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 27, '2020-07-10 20:58:43', '2020-07-10 20:58:43'),
(29, 'App\\Certificate', 13, 'certificate_logo', '5f092508622c6_3', '5f092508622c6_3.png', 'image/png', 'public', 5894, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 28, '2020-07-10 21:03:45', '2020-07-10 21:03:46'),
(30, 'App\\Certificate', 20, 'certificate_logo', '5f1940d6024c8_certificate_logo', '5f1940d6024c8_certificate_logo.png', 'image/png', 'public', 44575, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 29, '2020-07-23 02:19:12', '2020-07-23 02:19:30'),
(31, 'App\\Certificate', 20, 'certificate_signature', '5f1940df774c6_signature', '5f1940df774c6_signature.jpeg', 'image/jpeg', 'public', 52973, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 30, '2020-07-23 02:19:30', '2020-07-23 02:19:31');

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
