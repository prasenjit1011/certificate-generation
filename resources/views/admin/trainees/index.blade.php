@extends('layouts.admin')
@section('content')
@can('trainee_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.trainees.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.trainee.title_singular') }}
            </a>

        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.trainee.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Trainee">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.trainee.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.trainee.fields.emp_category') }}
                        </th>
                        <th>
                            {{ trans('cruds.trainee.fields.first_name') }}
                        </th>
                        <th>
                            {{ trans('cruds.trainee.fields.last_name') }}
                        </th>
                        <th>
                            {{ trans('cruds.trainee.fields.institution_name') }}
                        </th>
                        <th>
                            {{ trans('cruds.trainee.fields.emailid') }}
                        </th>
                        <th>
                            {{ trans('cruds.trainee.fields.contact_no') }}
                        </th>
                        <th>
                            {{ trans('cruds.trainee.fields.issue_date') }}
                        </th>
                        <th>
                            {{ trans('cruds.trainee.fields.serial_no') }}
                        </th>
                        <th>
                            {{ trans('cruds.trainee.fields.certificate_approval_status') }}
                        </th>
                        <th>
                            {{ trans('cruds.trainee.fields.certificate_approve_by') }}
                        </th>
                        <th>
                            {{ trans('cruds.trainee.fields.certificate_approve_date') }}
                        </th>
                        <th>
                            {{ trans('cruds.trainee.fields.certificate_qrcode') }}
                        </th>
                        <th>
                            {{ trans('cruds.trainee.fields.default_certificate') }}
                        </th>
                        <th>
                            {{ trans('cruds.trainee.fields.created_by') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($trainees as $key => $trainee)
                        <tr data-entry-id="{{ $trainee->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $trainee->id ?? '' }}
                            </td>
                            <td>
                                {{ App\Trainee::EMP_CATEGORY_SELECT[$trainee->emp_category] ?? '' }}
                            </td>
                            <td>
                                {{ $trainee->first_name ?? '' }}
                            </td>
                            <td>
                                {{ $trainee->last_name ?? '' }}
                            </td>
                            <td>
                                {{ $trainee->institution_name ?? '' }}
                            </td>
                            <td>
                                {{ $trainee->emailid ?? '' }}
                            </td>
                            <td>
                                {{ $trainee->contact_no ?? '' }}
                            </td>
                            <td>
                                {{ $trainee->issue_date ?? '' }}
                            </td>
                            <td>
                                {{ $trainee->serial_no ?? '' }}
                            </td>
                            <td>
                                {{ App\Trainee::CERTIFICATE_APPROVAL_STATUS_SELECT[$trainee->certificate_approval_status] ?? '' }}
                            </td>
                            <td>
                                {{ $trainee->certificate_approve_by ?? '' }}
                            </td>
                            <td>
                                {{ $trainee->certificate_approve_date ?? '' }}
                            </td>
                            <td>
                                {{ $trainee->certificate_qrcode ?? '' }}
                            </td>
                            <td>
                                {{ $trainee->default_certificate ?? '' }}
                            </td>
                            <td>
                                {{ $trainee->created_by ?? '' }}
                            </td>
                            <td>
                                @can('trainee_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.trainees.show', $trainee->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('trainee_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.trainees.edit', $trainee->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('trainee_delete')
                                    <form action="{{ route('admin.trainees.destroy', $trainee->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('trainee_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.trainees.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-Trainee:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection