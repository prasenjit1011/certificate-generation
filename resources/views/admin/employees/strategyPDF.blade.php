<?php
$data 	= file_get_contents('images/certificate_03.jpg');;//strategyCertificate.jpg
$data 	= file_get_contents('images/strategyCertificate.jpg');
$base64 = 'data:image/jpg;base64,' . base64_encode($data);

/*
شهادة تكريم
تتقدم الإدارة العامة للدفاع المدني - دبي بخالص الشكر والتقدير إلى
مدني / سید کا شف ء تتشا تشسسش 0110
تقديرا لجهوده الفعالة و تقديم اقتراحا مجديا يسهم
في تطوير العمل بما يحقق الأهداف الاستراتيجية
مع تمنياتنا له بالتوفيق والنجاح
العميد سيد ششکا سشيا تنا ۱

*/

?>
<!DOCTYPE html>
<html>
<head>
<title>{{$certificate['certificate_title']}}</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
body{	
	background: url(<?php echo $base64 ?>);
	font-family: DejaVu Sans, sans-serif;
	margin-header:0mm;
}
@page {
	margin:1px;
}
</style>
</head>
<body>
@if(0)
<h2 align="center" style="font-size:120px; margin-top:450px; color:#BF9140">
شهادة تكريم
</h2>
<h3 align="center" style="margin:140px 0px 0px 450px;  font-size:65px; color:#000; border:1px solid #F0F;  background-color:#FF4;">
	تتقدم الإدارة العامة للدفاع المدني - دبي بخالص الشكر والتقدير إلى
</h3>

<h2 align="center" style="margin:92px 50px 15px 600px; font-size:85px; color:#000; border:1px solid #F0F;  background-color:#F0F;">
	emp_category / last_name first_name serial_no
</h2>
<h2 align="center" style="font-size:60px;  margin-left:390px; margin-top:102px; color:#000;">
	تقديرا لجهوده الفعالة و تقديم اقتراحا مجديا يسهم
</h2>
<h2 align="center" style="font-size:60px;  margin-left:400px; margin-top:37px; color:#000;">
	في تطوير العمل بما يحقق الأهداف الاستراتيجية
</h2>
<h2 align="center" style="font-size:60px;  margin-left:400px; margin-top:32px; color:#000;">
	مع تمنياتنا له بالتوفيق والنجاح	
</h2>
@else	


	{!!str_replace('emp_category',$employee['emp_category'],str_replace('last_name',$employee['last_name'],str_replace('first_name',$employee['first_name'],str_replace('serial_no',$employee['serial_no'],str_replace('pdf_name_setup',$employee['pdf_name_setup'],$certificate['certificate_details'])))))!!}
	@if(isset($employee['certificate_approval_status']) && $employee['certificate_approval_status'] == 2)
		
@endif	
		<table width="81%" align="center" border="0"  style="margin-top:270px;">	
			<tr>
				<td width="50%">
					<h1  style="font-size:40px; margin-left:20px; margin-top:57px;color:#000;">
						{{$certificate['certificate_issue_by_post']}} / {{$certificate['certificate_issue_by_officername']}}				
					</h1>
					<h1 style="font-size:40px; margin-top:35px;color:#ccc;">
						{{$certificate['certificate_issue_msg']}}
					</h1>
				</td>
				<td align="left" >	
					<img src="{{ asset('images/qrcode/qrcode-').$qrstr}}.png" width="350"  style="padding-left:850px;" />
				</td>
			</tr>
		</table>
	@endif
</body>
</html>

