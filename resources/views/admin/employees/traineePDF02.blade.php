<?php
//$data 	= file_get_contents('images/cert-trai-1.jpg');
$data 	= file_get_contents('images/traineeCertificate2.jpg');
$base64 = 'data:image/jpg;base64,' . base64_encode($data);
?>
<!DOCTYPE html>
<html>
<head>
<title>{{$certificate['certificate_title']}}</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
body{
	background: url(<?php echo $base64; ?>);
	font-family: DejaVu Sans, sans-serif;
	margin-header:0mm;
}
@page {
	margin:1px;
}
</style>
</head>
<body>
<table width="100%" align="center" border="0" style="margin-top:55px;">
	<tr>
		<th align="center">
			<img src="{{ public_path() }}\images\certificate_logo.png" width="1300"  />
		</th>
	</tr>
</table>
<?php /*
<div style="background-color:#F00;">
	<div align="center" style="background-color:#F00; font-size:50px; text-align:center; margin-top:460px;">دبي بخالص الشكر والتقدير إلى</div>
	<h1 style="font-size:50px; text-align:center; margin-top:35px; ">ملازم أول رقم /338 / سعيد عبدالله</h1>
	<h1 style="font-size:30px; text-align:center; margin-top:9px;">قد اجتاز دورة الحس الأمني</h1>
	<h1 style="font-size:30px; text-align:center; margin-top:22px;">والمنعقدة في فندق الحبتور بولو۔ دہی</h1>
	<h1 style="font-size:30px; text-align:center; margin-top:25px;">من تاريخ 2019/07/07 م إلى 2019/07/11 م</h1>
	<h1 style="font-size:30px; text-align:center; margin-top:25px;">مع تمنياتام بالتوفيق</h1>
</div>
*/ ?>

	<h1 style="font-size:50px; margin-top:460px; margin-left:835px;">دبي بخالص الشكر والتقدير إلى</h1>
	<h1 style="font-size:45px; margin-top:35px;  margin-left:835px;">ملازم أول رقم /338 / سعيد عبدالله</h1>
	<h1 style="font-size:30px; margin-top:25px;  margin-left:1010px;">قد اجتاز دورة الحس الأمني</h1>
	<h1 style="font-size:30px; margin-top:25px;  margin-left:950px;">والمنعقدة في فندق الحبتور بولو۔ دہی</h1>
	<h1 style="font-size:30px; margin-top:30px;  margin-left:900px;">من تاريخ 2019/07/07 م إلى 2019/07/11 م</h1>
	<h1 style="font-size:30px; margin-top:14px;  margin-left:1050px;">مع تمنياتام بالتوفيق</h1>

@if(isset($employee['certificate_approve_by']) && $employee['certificate_approve_by']>0)	
	<?php /*<div style="height:650px;"></div><h1 style="font-size:50px; margin-top:47px;">{{$user[$employee['certificate_approve_by']]['signature_name']}} / {{$user[$employee['certificate_approve_by']]['name']}}</h1>
	<h1 style="font-size:50px; margin-top:35px;">{{$user[$employee['certificate_approve_by']]['department']}}</h1>*/ ?>
	<h1 style="font-size:35px; margin-top:35px; margin-left:835px;">/ العميد</h1>
	<h1 style="font-size:35px; margin-top:5px; margin-left:835px;">سید ششكا سيشيا تنا</h1>
	<h1 style="font-size:35px; margin-top:5px; margin-left:835px;">مدير عام مدير عام</h1>
	<img src="images/expo-logo.jpg" width="286" style="float:left; margin-left:37px;  margin-top:70px; " />
	<img src="{{ public_path() }}\images\qrcode\qrcode-{{$qrstr}}.png" width="200" style="float:right; margin-right:215px;  border:2px solid #f00;" />
@endif
</body>
</html>

