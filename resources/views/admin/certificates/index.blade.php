@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('cruds.certificate.title_singular') }} {{ trans('global.list') }}
		
@can('certificate_create')

            <a class="btn btn-success float-right" href="{{ route('admin.certificates.create') }}">
                {{ trans('global.add') }} <?php /*{{ trans('cruds.certificate.title_singular') }}*/ ?>
            </a>
@endcan		
		
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Certificate">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.certificate.fields.id') }}
                        </th>
                        <?php /*<th>
                            {{ trans('cruds.certificate.fields.template') }}
                        </th>
						
                        <th>
                            {{ trans('global.ctype') }}
                        </th>
						*/ ?>
                        <th>
                            {{ trans('cruds.user.fields.department') }}
                        </th>
                        <th>
                            {{ trans('cruds.certificate.fields.certificate_title') }}
                        </th>
                        <th>
                            {{ trans('cruds.certificate.fields.certificate_subtitle') }}
                        </th>
                        <?php /*<th>
                            {{ trans('cruds.certificate.fields.certificate_logo') }}
                        </th>*/?>                        
                        <th>
                            {{ trans('global.action') }}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($certificates as $key => $certificate)
                        <tr data-entry-id="{{ $certificate->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $certificate->id ?? '' }}
                            </td>
                            <?php /*<td>								
                                <?php /*{{ App\Certificate::TEMPLATE_RADIO[$certificate->template] ?? '' }}* ?>
								<img src="{{asset('/images/template'.$certificate->template.'.jpg')}}" width="100" style="margin:10px; " />
                            </td>
							
                            <td>
								@if($certificate->type == 1)
									General
								@elseif($certificate->type == 2)
									Strategy
								@elseif($certificate->type == 3)
									Trainee
								@endif
                            </td>
							*/ ?>
                            <td>
                                {{ $certificate->department ?? '' }}
                            </td>
                            <td>
                                {{ $certificate->certificate_title ?? '' }}
                            </td>
                            <td>
                                {{ $certificate->certificate_subtitle ?? '' }}
                            </td>
                            <?php /*
							<td>
								
                                @if($certificate->certificate_logo)
									@php($certificateImgURL = $certificate->certificate_logo->getUrl('thumb'))
									@php($certificateLargeImgURL = $certificate->certificate_logo->getUrl())
									<a href="{{ $certificateLargeImgURL }}" target="_blank">
                                        <img src="{{ $certificateImgURL }}" width="50px" height="50px">
                                    </a>
                                @endif
                            </td>*/?>
                            <td>
                                @can('certificate_show')
                                    <a class="btn btn-xs btn-primary d-none" href="{{ route('admin.certificates.show', $certificate->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('certificate_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.certificates.edit', $certificate->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('certificate_delete')
                                    <form action="{{ route('admin.certificates.destroy', $certificate->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block; display:none;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('certificate_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.certificates.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-Certificate:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection