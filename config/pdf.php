<?php

return [
	'mode'                  => 'utf-8',
	'format'                => [256,177],//A4, A4-L, [1024,709], [256,177] -- default
	'author'                => '',
	'subject'               => '',
	'keywords'              => '',
	'margin-header'			=> '0',
	'creator'               => 'prasenjit.aluni@gmail.com',
	'display_mode'          => 'fullpage',
	'tempDir'               => base_path('storage/mpdf/'),
	
	'arabic-font' => [
						'R'  => 'arabic-font.ttf',    // regular font
						'B'  => 'arabic-font.ttf',          // optional: bold font
						'I'  => 'arabic-font-Light.ttf',    // optional: italic font
						'BI' => 'arabic-font.ttf',           // optional: bold-italic font
						'useOTL' => 0xFF,
						'useKashida' => 75,
					]
];
