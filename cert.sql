-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2020 at 01:45 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newcertificatedevopment`
--

-- --------------------------------------------------------

--
-- Table structure for table `certificates`
--

CREATE TABLE `certificates` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` tinyint(4) DEFAULT 1 COMMENT '1:general, 2:stategy, 3:trainee',
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` int(11) NOT NULL DEFAULT 1,
  `certificate_issue_by_post` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_issue_by_officername` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_issue_msg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `certificate_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `certificate_subtitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_details1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_details2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `empid` int(11) DEFAULT NULL,
  `serial_prefix` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serial_no` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `certificates`
--

INSERT INTO `certificates` (`id`, `type`, `department`, `template`, `certificate_issue_by_post`, `certificate_issue_by_officername`, `certificate_issue_msg`, `certificate_title`, `certificate_subtitle`, `certificate_details`, `certificate_details1`, `certificate_details2`, `created_by`, `empid`, `serial_prefix`, `serial_no`, `created_at`, `updated_at`, `deleted_at`) VALUES
(19, 1, 'Strategy Department', 1, 'العميد', 'سيد سالم امين', 'رئيس اللجنة العليا للمقترحات والشكاوي', 'شهادة تكريم', NULL, '<h2 align=\"center\" style=\"font-size:120px; margin-top:450px; color:#BF9140\">\r\nشهادة تكريم\r\n</h2>\r\n<h3 align=\"center\" style=\"margin:140px 0px 0px 450px;  font-size:65px; \">\r\n	تتقدم الإدارة العامة للدفاع المدني - دبي بخالص الشكر والتقدير إلى\r\n</h3>\r\n\r\n<h2 align=\"center\" style=\"pdf_name_setup\">\r\n	emp_category / last_name first_name serial_no\r\n</h2>\r\n<h2 align=\"center\" style=\"font-size:60px;  margin-left:390px; margin-top:102px; color:#000;\">\r\n	تقديرا لجهوده الفعالة و تقديم اقتراحا مجديا يسهم\r\n</h2>\r\n<h2 align=\"center\" style=\"font-size:60px;  margin-left:400px; margin-top:37px; color:#000;\">\r\n	في تطوير العمل بما يحقق الأهداف الاستراتيجية\r\n</h2>\r\n<h2 align=\"center\" style=\"font-size:60px;  margin-left:400px; margin-top:32px; color:#000;\">\r\n	مع تمنياتنا له بالتوفيق والنجاح	\r\n</h2>', NULL, NULL, 42, 0, NULL, 103, '2020-07-23 00:37:51', '2020-08-04 06:11:44', NULL),
(20, 1, 'Trainee Department', 2, 'العميد', 'السيد ششكا سشیا تنا۱', 'مدير عام مدير عام', 'دبي بخالص الشكر والتقدير إلى', NULL, '<p style=\"font-size:30px; color:#BF9140; text-align:right; width:650px; margin:330px auto 0px; border:0px solid #FF0;\">\r\nدبي بخالص الشكر والتقدير إلى\r\n</p>\r\n<p align=\"center\" style=\"pdf_name_setup\">\r\n	emp_category  first_name last_name	\r\n</p>\r\n<p style=\"font-size:20px; font-color:#F00; text-align:center; width:650px; margin:5px auto 0px; padding-left: 50px; border:0px solid #FF0;\">\r\nقد اجتاز دورة الحس الأمني\r\n</p>\r\n<p style=\"font-size:20px; font-color:#F00; text-align:center; width:650px; margin:1px auto 0px; padding-left: 120px; border:0px solid #FF0;\">\r\nوالمنعقدة في فندق الحبتور بولو۔ دہی\r\n</p>\r\n<p style=\"font-size:20px; font-color:#F00; text-align:center; width:650px; margin:1px auto 0px; padding-left: 10px; border:0px solid #FF0;\">\r\nfrom_date - to_date\r\n</p>\r\n<p style=\"font-size:20px; font-color:#F00; text-align:center; width:650px; margin:1px auto 0px; padding-left: 100px; border:0px solid #FF0;\">\r\nمع تمنياتام بالتوفيق\r\n</p>', NULL, NULL, 43, 0, 'EIT', 1545, '2020-07-23 00:37:04', '2020-08-04 03:06:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `crud`
--

CREATE TABLE `crud` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cruds`
--

CREATE TABLE `cruds` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `department`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'EngDept', '2020-06-15 18:30:00', '2020-07-23 00:09:27', '2020-07-23 00:09:27'),
(2, 'MedDept', '2020-06-25 23:29:22', '2020-07-23 00:10:01', '2020-07-23 00:10:01'),
(3, 'CvilManager', '2020-06-25 23:38:14', '2020-07-23 00:09:50', '2020-07-23 00:09:50'),
(4, 'StrategyDept', '2020-07-19 07:17:54', '2020-07-23 00:10:13', '2020-07-23 00:10:13'),
(5, 'TraningDept', '2020-07-19 07:19:26', '2020-07-23 00:09:39', '2020-07-23 00:09:39'),
(6, 'StraDept', '2020-07-21 09:45:07', '2020-07-23 00:10:24', '2020-07-23 00:10:24'),
(7, 'TraDept', '2020-07-21 09:45:21', '2020-07-23 00:10:35', '2020-07-23 00:10:35'),
(8, 'Strategy Department', '2020-07-23 00:02:16', '2020-07-23 00:02:16', NULL),
(9, 'Trainee Department', '2020-07-23 00:02:48', '2020-07-23 00:02:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_type` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1:general, 2:stategy, 3:trainee',
  `emp_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emailid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_no` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `courseduration` varchar(35) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `reviewed_by` int(11) NOT NULL DEFAULT 0,
  `institution_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_certificate` int(11) DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `serial_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_approve_by` int(11) DEFAULT 0,
  `certificate_qrcode` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_approve_date` date DEFAULT '2020-01-01',
  `employee_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `certificate_approval_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '1' COMMENT '1:Pending, 2:Approved, 4:Review, 3:Cancel',
  `emp_category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `pdf_generated` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0:Not Generated, 1:Generated',
  `pdf_name_setup` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;',
  `mail_sent` tinyint(4) NOT NULL DEFAULT 0,
  `mail_sent_schedule` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `emp_type`, `emp_id`, `emailid`, `contact_no`, `first_name`, `last_name`, `courseduration`, `created_by`, `reviewed_by`, `institution_name`, `department`, `default_certificate`, `issue_date`, `serial_no`, `certificate_approve_by`, `certificate_qrcode`, `certificate_approve_date`, `employee_type`, `certificate_approval_status`, `emp_category`, `pdf_generated`, `pdf_name_setup`, `mail_sent`, `mail_sent_schedule`, `created_at`, `updated_at`, `deleted_at`) VALUES
(20, 1, NULL, 'prasenjit.aluni@gmail.com', NULL, 'سعيد عبدالله', 'سيد', '11/07/2020 - 30/08/2020', 43, 0, 'United String Technology', 'Trainee Department', 20, '2020-08-09', '152', 43, NULL, NULL, '2', '2', '1', 0, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, '2020-07-29 22:36:44', '2020-07-31 19:52:18', NULL),
(21, 1, 'fhfg', 'prasenjit.aluni@gmail.com', NULL, 'سعيد عبدالله', 'سيد', '11/07/2020 - 30/08/2020', 43, 0, NULL, 'Trainee Department', 20, '2020-08-30', 'dgfg', 43, NULL, NULL, '1', '2', '1', 0, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, '2020-07-29 22:37:20', '2020-08-02 04:00:01', NULL),
(22, 1, NULL, 'prasenjit.aluni@gmail.com', NULL, 'سعيد عبدالله', 'سيد', '11/07/2020 - 30/08/2020', 42, 0, 'United String Technology', 'Strategy Department', 19, '2020-08-07', 'esfsdf', 0, NULL, NULL, '2', '1', '1', 0, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, '2020-07-29 22:38:36', '2020-07-29 22:38:36', NULL),
(23, 1, 'fgh', 'prasenjit.aluni@gmail.com', NULL, 'سعيد عبدالله', 'سيد', '11/07/2020 - 30/08/2020', 42, 0, NULL, 'Strategy Department', 19, '2020-08-08', 'cbv', 0, NULL, NULL, '1', '4', '1', 0, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, '2020-07-29 22:38:51', '2020-07-29 22:38:51', NULL),
(24, 1, '65412', 'riyad@admin.com', NULL, 'سعيد عبدالله', 'سيد', '11/07/2020 - 30/08/2020', 42, 0, NULL, 'Strategy Department', 19, '2020-07-31', 'Test123', 0, NULL, '2020-01-01', '1', '1', '1', 0, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, NULL, NULL, NULL),
(25, 1, '4544', 'prasenjit.aluni@gmail.com', NULL, 'سعيد عبدالله', 'سيد', '11/07/2020 - 30/08/2020', 43, 0, NULL, 'Trainee Department', 20, '2020-08-30', 'dxcgdfgd', 43, NULL, '2020-01-01', '1', '2', '1', 1, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, NULL, '2020-08-02 04:49:31', NULL),
(26, 1, NULL, NULL, NULL, 'سعيد عبدالله', 'سيد', '11/07/2020 - 30/08/2020', 44, 0, 'dgdfg', 'Strategy Department', 19, '2020-09-06', 's324', 42, NULL, NULL, '2', '2', '1', 1, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 1, 0, '2020-07-29 23:21:22', '2020-08-02 02:40:07', NULL),
(27, 1, NULL, NULL, NULL, 'سعيد عبدالله', 'سيد', '11/07/2020 - 30/08/2020', 44, 0, 'cvc', 'Trainee Department', 20, '2020-08-07', 'xcvc', 43, NULL, NULL, '2', '2', '1', 0, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, '2020-07-29 23:38:58', '2020-07-31 20:18:34', NULL),
(28, 1, NULL, NULL, NULL, 'سعيد عبدالله', 'سيد', '11/07/2020 - 30/08/2020', 44, 0, 'fhfh', 'Strategy Department', 19, '2020-08-08', 'fghf', 42, NULL, NULL, '2', '2', '1', 1, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 1, 0, '2020-07-29 23:39:10', '2020-08-04 03:43:12', NULL),
(29, 1, 'ffgf', 'test@gmail.com', NULL, 'سعيد عبدالله', 'سيد', '2011/07/20 - 2030/08/20', 44, 0, NULL, 'Trainee Department', 20, '2020-08-09', 'dfgd', 43, NULL, NULL, '1', '2', '1', 1, 'width:830px; margin:1px 1px 1px 180px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, '2020-07-29 23:40:48', '2020-08-04 02:42:01', NULL),
(30, 1, 'dfg', NULL, NULL, 'سعيد عبدالله', 'سيد', '11/07/2020 - 30/08/2020', 44, 0, NULL, 'Strategy Department', 19, '2020-08-09', 'dfg', 42, NULL, NULL, '1', '2', '1', 0, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, '2020-07-29 23:41:03', '2020-07-29 23:49:59', NULL),
(31, 1, NULL, 'prasenjit.aluni@gmail.com', NULL, 'سعيد عبدالله', 'سيد', '11/07/2020 - 30/08/2020', 43, 0, 'United String Technology', 'Trainee Department', 20, '2020-08-14', '4334', 43, NULL, NULL, '2', '2', '1', 0, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, '2020-07-31 20:33:45', '2020-07-31 21:11:01', NULL),
(32, 1, NULL, 'prasenjit.aluni@gmail.com', NULL, 'سعيد عبدالله', 'سيد', '11/07/2020 - 30/08/2020', 43, 0, 'United String Technology', 'Trainee Department', 20, '2020-08-26', '34543', 43, NULL, NULL, '2', '2', '1', 0, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, '2020-07-31 20:33:57', '2020-08-01 08:03:32', NULL),
(33, 1, NULL, 'prasenjit.aluni@gmail.com', NULL, 'سعيد عبدالله', 'سيد', '11/07/2020 - 30/08/2020', 43, 0, 'United String Technology', 'Trainee Department', 20, '2020-08-30', '65465', 43, NULL, NULL, '2', '2', '1', 0, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, '2020-07-31 20:34:11', '2020-07-31 21:47:52', NULL),
(34, 1, NULL, 'prasenjit.aluni@gmail.com', NULL, 'سعيد عبدالله', 'سيد', '11/07/2020 - 30/08/2020', 43, 0, 'United String Technology', 'Trainee Department', 20, '2020-08-15', '998', 43, NULL, NULL, '2', '2', '1', 0, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, '2020-07-31 21:32:49', '2020-07-31 21:33:20', NULL),
(35, 1, NULL, 'prasenjit.aluni@gmail.com', NULL, 'سعيد عبدالله', 'سيد', '11/07/2020 - 30/08/2020', 43, 0, 'United String Technology', 'Trainee Department', 20, '2020-09-05', '432', 43, NULL, NULL, '2', '2', '1', 0, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, '2020-07-31 22:02:19', '2020-07-31 22:04:30', NULL),
(36, 1, NULL, 'prasenjit.aluni@gmail.com', NULL, 'سعيد عبدالله', 'سيد', '11/07/2020 - 30/08/2020', 43, 0, 'United String Technology', 'Trainee Department', 20, '2020-09-05', '4324', 0, NULL, NULL, '2', '2', '1', 0, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, '2020-07-31 22:02:39', '2020-07-31 23:44:23', NULL),
(37, 1, NULL, 'prasenjit.aluni@gmail.com', NULL, 'سعيد عبدالله', 'سيد', '11/07/2020 - 30/08/2020', 43, 0, 'United String Technology', 'Trainee Department', 20, '2020-08-29', '456', 43, NULL, NULL, '2', '2', '1', 0, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, '2020-07-31 22:03:15', '2020-08-01 23:23:01', NULL),
(38, 1, NULL, 'prasenjit.aluni@gmail.com', NULL, 'سعيد عبدالله', 'سيد', '11/07/2020 - 30/08/2020', 43, 0, 'United String Technology', 'Trainee Department', 20, '2020-08-08', '4564', 43, NULL, NULL, '2', '2', '1', 1, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, '2020-08-02 01:33:03', '2020-08-03 22:41:01', NULL),
(39, 1, NULL, 'prasenjit.aluni@gmail.com', NULL, 'سعيد عبدالله', 'سيد', '11/07/2020 - 30/08/2020', 43, 0, 'United String Technology', 'Trainee Department', 20, '2020-08-28', '9856', 43, NULL, NULL, '2', '2', '1', 1, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 1, 0, '2020-08-02 01:56:19', '2020-08-04 01:21:21', NULL),
(40, 1, NULL, 'prasenjit.aluni@gmail.com', NULL, 'سعيد عبدالله', 'سيد', '2011/07/20 - 2030/08/20', 42, 0, 'United String Technology', 'Strategy Department', 19, '2020-09-04', '103', 42, NULL, NULL, '2', '2', '1', 1, 'margin:92px 50px 15px 300px; font-size:85px; color:#000;', 0, 0, '2020-08-02 03:23:24', '2020-08-04 06:13:39', NULL),
(41, 1, '01987', 'kashif.mis@gmail.com', NULL, 'شسميتمشستيمنتسش', 'شتسيمشتسيسمنيسش', '11/07/2020 - 30/08/2020', 46, 45, NULL, 'Trainee Department', 20, '2020-08-02', '78547', 43, NULL, NULL, '1', '2', '1', 1, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, '2020-08-02 03:46:23', '2020-08-02 04:07:57', NULL),
(42, 1, NULL, 'kashif.mis@gmail.com', NULL, 'شسانياسشيانت', 'اشنتياشستاينتسشاي', '2011/08/08 - 2011/08/24', 46, 0, 'ajsdjsakjd company ltd', 'Trainee Department', 20, '2020-09-01', '34344', 43, NULL, NULL, '2', '2', '1', 1, 'width:800px; margin:1px 1px 1px 270px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, '2020-08-02 03:52:22', '2020-08-04 02:42:21', NULL),
(43, 1, '24242', 'kashif.mis@gmail.com', NULL, 'ىيسشيشساي', 'شتسيات', '2011/07/20 - 2030/08/20', 43, 0, NULL, 'Trainee Department', 20, '2020-08-02', '23232', 43, NULL, NULL, '1', '2', '1', 1, 'width:800px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 1, 0, '2020-08-02 04:10:09', '2020-08-04 02:42:31', NULL),
(44, 1, '78541', 'prasenjit.aluni@gmail.com', NULL, 'تتتنال', 'تتا', '2011/07/20 - 2030/08/20', 43, 0, NULL, 'Trainee Department', 20, '2020-08-02', '1545', 43, NULL, NULL, '1', '2', '1', 1, 'width:830px; margin:1px 1px 1px 220px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, '2020-08-02 04:20:53', '2020-08-04 02:37:11', NULL),
(45, 1, NULL, 'prasenjit.aluni@gmail.com', NULL, 'سيد علي حسيني خامنئي', 'سيد علي حسيني خامنئي', '2020/08/04 - 2020/08/04', 43, 0, 'United String Technology', 'Trainee Department', 20, '2020-08-04', NULL, 43, NULL, NULL, '2', '2', '1', 1, 'width:850px; margin:1px 1px 1px 230px; padding:10px 1px 0px 10px; font-size:30px; text-align:center;', 0, 0, '2020-08-04 01:53:09', '2020-08-04 03:10:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `internalmembers`
--

CREATE TABLE `internalmembers` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_type` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1:general, 2:stategy, 3:trainee',
  `emp_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `institution_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emailid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_approval_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_approve_by` int(11) DEFAULT NULL,
  `certificate_approve_date` date DEFAULT '2020-01-01',
  `certificate_qrcode` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_certificate` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `employee_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `internalmembers`
--

INSERT INTO `internalmembers` (`id`, `emp_type`, `emp_id`, `emp_category`, `first_name`, `last_name`, `institution_name`, `emailid`, `contact_no`, `department`, `certificate_approval_status`, `certificate_approve_by`, `certificate_approve_date`, `certificate_qrcode`, `default_certificate`, `created_by`, `employee_type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '8965', '1', 'EMP 04', 'TRAINEE', NULL, NULL, NULL, 'Trainee Department', NULL, NULL, '2020-07-23', NULL, NULL, NULL, NULL, '2020-07-23 01:34:18', '2020-07-23 01:34:18', NULL),
(2, 1, '8995', '1', 'EMP 06', 'TRAINEE', NULL, 'emp06@admin.com', '9852114552', 'Trainee Department', NULL, NULL, '2020-07-23', NULL, NULL, NULL, NULL, '2020-07-23 01:35:57', '2020-07-23 01:35:57', NULL),
(3, 1, '4122', '1', 'EMP 09', 'TRAINEE', NULL, 'emp09@admin.com', '4456622112', 'Trainee Department', NULL, NULL, '2020-07-23', NULL, NULL, NULL, NULL, '2020-07-23 01:37:00', '2020-07-23 01:37:00', NULL),
(4, 0, '65412', '1', 'Riyad', 'STRATEGY', NULL, 'riyad@admin.com', '35345345', 'Strategy Department', NULL, NULL, '2020-07-25', NULL, NULL, NULL, NULL, '2020-07-24 23:30:26', '2020-07-24 23:30:26', NULL),
(5, 0, '34554', '1', 'Aklak', 'EMPTR', NULL, 'alkal@admin.com', '353453454', 'Trainee Department', NULL, NULL, '2020-07-25', NULL, NULL, NULL, NULL, '2020-07-24 23:31:45', '2020-07-24 23:31:45', NULL),
(6, 1, '4544', '1', 'Prasenjit', 'Aluni', NULL, 'prasenjit.aluni@gmail.com', NULL, 'Trainee Department', NULL, NULL, '2020-01-01', NULL, NULL, NULL, NULL, '2020-07-29 22:46:26', '2020-07-29 22:46:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `institution_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emailid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_approval_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_approve_by` int(11) DEFAULT NULL,
  `certificate_approve_date` date DEFAULT NULL,
  `certificate_qrcode` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_certificate` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `employee_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(3, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(5, '2016_06_01_000004_create_oauth_clients_table', 1),
(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2020_06_15_000002_create_permissions_table', 1),
(9, '2020_06_15_000003_create_roles_table', 1),
(10, '2020_06_15_000004_create_users_table', 1),
(11, '2020_06_15_000005_create_employees_table', 1),
(12, '2020_06_15_000006_create_certificates_table', 1),
(13, '2020_06_15_000007_create_permission_role_pivot_table', 1),
(14, '2020_06_15_000008_create_role_user_pivot_table', 1),
(15, '2020_06_16_000007_create_departments_table', 2),
(16, '2020_06_18_000008_create_members_table', 3),
(17, '2020_06_25_000008_create_members_table', 4),
(18, '2020_06_25_000010_create_internalmembers_table', 5),
(19, '2020_06_19_000005_create_trainees_table', 6),
(20, '2020_07_03_033228_create_crud_table', 6),
(21, '2020_07_03_033911_create_cruds_table', 6),
(22, '2020_07_19_000011_create_trainees_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'user_management_access', NULL, NULL, NULL),
(2, 'permission_create', NULL, NULL, NULL),
(3, 'permission_edit', NULL, NULL, NULL),
(4, 'permission_show', NULL, NULL, NULL),
(5, 'permission_delete', NULL, NULL, NULL),
(6, 'permission_access', NULL, NULL, NULL),
(7, 'role_create', NULL, NULL, NULL),
(8, 'role_edit', NULL, NULL, NULL),
(9, 'role_show', NULL, NULL, NULL),
(10, 'role_delete', NULL, NULL, NULL),
(11, 'role_access', NULL, NULL, NULL),
(12, 'user_create', NULL, NULL, NULL),
(13, 'user_edit', NULL, NULL, NULL),
(14, 'user_show', NULL, NULL, NULL),
(15, 'user_delete', NULL, NULL, NULL),
(16, 'user_access', NULL, NULL, NULL),
(17, 'employee_create', NULL, NULL, NULL),
(18, 'employee_edit', NULL, NULL, NULL),
(19, 'employee_show', NULL, NULL, NULL),
(20, 'employee_delete', NULL, NULL, NULL),
(21, 'employee_access', NULL, NULL, NULL),
(22, 'certificate_create', NULL, NULL, NULL),
(23, 'certificate_edit', NULL, NULL, NULL),
(24, 'certificate_show', NULL, NULL, NULL),
(25, 'certificate_delete', NULL, NULL, NULL),
(26, 'certificate_access', NULL, NULL, NULL),
(27, 'profile_password_edit', NULL, NULL, NULL),
(28, 'member_create', NULL, NULL, NULL),
(29, 'member_edit', NULL, NULL, NULL),
(30, 'member_show', NULL, NULL, NULL),
(31, 'member_delete', NULL, NULL, NULL),
(32, 'employee_access', NULL, NULL, NULL),
(33, 'internal_employee_listing', '2020-06-25 00:00:32', '2020-06-25 00:00:32', NULL),
(34, 'internalmember_access', '2020-06-25 01:39:30', '2020-06-25 01:39:30', NULL),
(35, 'internalmember_create', '2020-06-25 01:53:00', '2020-06-25 01:53:00', NULL),
(36, 'internalmember_edit', '2020-06-25 01:53:10', '2020-06-25 01:53:10', NULL),
(37, 'internalmember_delete', '2020-06-25 01:53:21', '2020-06-25 01:53:21', NULL),
(38, 'internalmember_show', '2020-06-25 01:53:34', '2020-06-25 01:53:34', NULL),
(39, 'department_access', '2020-06-25 23:16:41', '2020-06-25 23:27:56', NULL),
(40, 'department_create', '2020-06-25 23:17:20', '2020-06-25 23:29:00', NULL),
(41, 'department_edit', '2020-06-25 23:17:31', '2020-06-25 23:28:12', NULL),
(42, 'department_delete', '2020-06-25 23:17:43', '2020-06-25 23:28:24', NULL),
(43, 'department_show', '2020-06-25 23:17:55', '2020-06-25 23:27:42', NULL),
(44, 'trainee_access', '2020-07-19 08:48:58', '2020-07-19 08:48:58', NULL),
(45, 'trainee_create', '2020-07-19 08:50:06', '2020-07-19 08:50:06', NULL),
(46, 'trainee_edit', '2020-07-19 08:50:15', '2020-07-19 08:50:15', NULL),
(47, 'trainee_delete', '2020-07-19 08:50:27', '2020-07-19 08:50:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `role_id` int(10) UNSIGNED NOT NULL DEFAULT 7,
  `permission_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`role_id`, `permission_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(2, 17),
(2, 18),
(2, 19),
(2, 20),
(2, 21),
(2, 22),
(2, 23),
(2, 24),
(2, 25),
(2, 26),
(2, 27),
(3, 1),
(3, 12),
(3, 13),
(3, 14),
(3, 15),
(3, 16),
(3, 17),
(3, 18),
(3, 19),
(3, 20),
(3, 21),
(3, 22),
(3, 23),
(3, 24),
(3, 25),
(3, 26),
(3, 27),
(4, 17),
(4, 18),
(4, 19),
(4, 20),
(4, 21),
(4, 24),
(4, 26),
(4, 27),
(5, 17),
(5, 18),
(5, 19),
(5, 20),
(5, 21),
(5, 24),
(5, 26),
(5, 27),
(4, 14),
(4, 16),
(4, 12),
(4, 13),
(4, 15),
(4, 1),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(5, 28),
(5, 29),
(5, 30),
(5, 31),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 38),
(5, 33),
(5, 34),
(5, 35),
(5, 36),
(5, 37),
(5, 38),
(4, 33),
(4, 34),
(4, 35),
(4, 36),
(4, 37),
(4, 38),
(3, 33),
(3, 34),
(3, 35),
(3, 36),
(3, 37),
(3, 38),
(1, 39),
(1, 40),
(1, 41),
(1, 42),
(1, 43),
(7, 28),
(7, 29),
(7, 30),
(7, 31),
(1, 44),
(1, 45),
(1, 46),
(1, 47),
(7, 17),
(7, 18),
(7, 19),
(7, 21),
(7, 44),
(7, 45),
(7, 26),
(9, 17),
(9, 18),
(9, 19),
(9, 20),
(9, 21),
(6, 1),
(6, 12),
(6, 13),
(6, 14),
(6, 15),
(6, 16),
(6, 17),
(6, 18),
(6, 19),
(6, 20),
(6, 21),
(6, 23),
(6, 26),
(6, 32),
(6, 34),
(6, 35),
(6, 36),
(6, 37),
(6, 38);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', NULL, NULL, NULL),
(2, 'User', NULL, NULL, NULL),
(3, 'HOD', '2020-06-14 22:12:30', '2020-06-18 23:52:07', NULL),
(4, 'Manager', '2020-06-16 00:31:00', '2020-06-18 23:52:23', NULL),
(5, 'Officer', '2020-06-16 01:39:37', '2020-06-18 23:52:39', NULL),
(6, 'Principal', '2020-06-27 04:13:16', '2020-06-27 04:13:16', NULL),
(7, 'TraineeOfficer', '2020-07-19 07:30:52', '2020-07-20 05:14:21', NULL),
(9, 'StrategyOfficer', '2020-07-20 05:49:22', '2020-07-20 06:12:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(42, 3),
(43, 3),
(44, 6),
(45, 4),
(46, 5);

-- --------------------------------------------------------

--
-- Table structure for table `trainees`
--

CREATE TABLE `trainees` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `institution_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emailid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `serial_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_approval_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_approve_by` int(11) DEFAULT NULL,
  `certificate_approve_date` date DEFAULT NULL,
  `certificate_qrcode` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_certificate` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `employee_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_type` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1:general, 2:stategy, 3:trainee',
  `created_by` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_type`, `created_by`, `name`, `signature_name`, `department`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 0, 'Admin', NULL, NULL, 'admin', NULL, '$2y$10$KjXReev6r5pHvflCEqyPM.dZ2YvsPlbQA5EbDJEkd6vLkMR3O/QZi', 'L5apPxnjuSfUu2mYpLAn9YaUQQWcnzsCGtL1bYvOcDrPbK5gSsp9s6YrgiIU', NULL, '2020-06-26 00:02:09', NULL),
(42, 1, 1, 'STRATEGY HOD', 'استيباتسيب انتسيابسي', 'Strategy Department', 'hodofstrategy', NULL, '$2y$10$qOi1QJwrH1a0dQhuE3Q8berx6FDhDwORloIkoBNSBL.BTPoKj5qfm', NULL, '2020-07-23 00:15:41', '2020-07-23 00:15:41', NULL),
(43, 1, 1, 'Trainee HOD', NULL, 'Trainee Department', 'hodoftrainee', NULL, '$2y$10$.WYWR5wpQejpA18At3XBdulUgMtbZcULnK1AbYKVUlmbJdULtqFAe', NULL, '2020-07-23 00:17:46', '2020-07-23 00:17:46', NULL),
(44, 1, 1, 'Principal', NULL, NULL, 'principal', NULL, '$2y$10$7CZV0TuTbuRomtCOfwqZ/.UgCovNMS92gXRNmklPFnmRqWUheoYj6', NULL, '2020-07-23 00:24:53', '2020-07-23 00:24:53', NULL),
(45, 1, 43, 'Trainee Manager', NULL, 'Trainee Department', 'manageroftrainee', NULL, '$2y$10$d74AtStPTm6TFGVQ1Uganu535P4XSYbXJtHftz74dMIE89hwsNiJW', NULL, '2020-07-23 00:56:55', '2020-07-23 00:56:55', NULL),
(46, 1, 45, 'Trainee Officer', NULL, 'Trainee Department', 'officeroftrainee', NULL, '$2y$10$Fz4z9706ojOPt4a.SGIBBOOBtpqhdKnAklCqqv9MZW0ilFJRiaCvW', NULL, '2020-07-23 01:00:20', '2020-07-23 01:00:20', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `certificates`
--
ALTER TABLE `certificates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crud`
--
ALTER TABLE `crud`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cruds`
--
ALTER TABLE `cruds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `internalmembers`
--
ALTER TABLE `internalmembers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD KEY `role_id_fk_1645052` (`role_id`),
  ADD KEY `permission_id_fk_1645052` (`permission_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD KEY `user_id_fk_1645061` (`user_id`),
  ADD KEY `role_id_fk_1645061` (`role_id`);

--
-- Indexes for table `trainees`
--
ALTER TABLE `trainees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `certificates`
--
ALTER TABLE `certificates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `crud`
--
ALTER TABLE `crud`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cruds`
--
ALTER TABLE `cruds`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `internalmembers`
--
ALTER TABLE `internalmembers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `trainees`
--
ALTER TABLE `trainees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_id_fk_1645052` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_id_fk_1645052` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_id_fk_1645061` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_id_fk_1645061` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
