<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_type' => 0,
                'created_by' => 0,
                'name' => 'Admin',
                'signature_name' => NULL,
                'department' => NULL,
                'email' => 'admin@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$KjXReev6r5pHvflCEqyPM.dZ2YvsPlbQA5EbDJEkd6vLkMR3O/QZi',
                'remember_token' => 'Kz6fvkE2g77eBGaQqwgOy0NzjDNyO9rHhrvRrWHsn27xX9jGAriulr4PlItJ',
                'created_at' => NULL,
                'updated_at' => '2020-06-26 05:32:09',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 31,
                'user_type' => 1,
                'created_by' => 1,
                'name' => 'GHOD',
                'signature_name' => 'استيباتسيب انتسيابسي',
                'department' => 'MedDept',
                'email' => 'ghod@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$E3WZ4.yrtKVgvnL4X7Vcbu9RmO2vQuwJnEQdULgIkPGgu1vgm4FZu',
                'remember_token' => NULL,
                'created_at' => '2020-07-21 17:46:56',
                'updated_at' => '2020-07-23 05:35:45',
                'deleted_at' => '2020-07-23 05:35:45',
            ),
            2 => 
            array (
                'id' => 32,
                'user_type' => 2,
                'created_by' => 1,
                'name' => 'SHOD',
                'signature_name' => 'استيباتسيب انتسيابسي',
                'department' => 'CvilManager',
                'email' => 'shod@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$RnGV5bDB2ew74OhqzdXkm.bgEWwNSFBbOzZRSVvm0yr3NhC0fQkV2',
                'remember_token' => NULL,
                'created_at' => '2020-07-21 17:48:08',
                'updated_at' => '2020-07-23 05:35:17',
                'deleted_at' => '2020-07-23 05:35:17',
            ),
            3 => 
            array (
                'id' => 33,
                'user_type' => 2,
                'created_by' => 32,
                'name' => 'SMG',
                'signature_name' => 'استيباتسيب انتسيابسي',
                'department' => 'CvilManager',
                'email' => 'smg@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$uPSl.sGmc01hf8YV6vLakeJ0QC8kVThJEvfPdAxnYtdtPLZICtR6q',
                'remember_token' => NULL,
                'created_at' => '2020-07-21 17:49:44',
                'updated_at' => '2020-07-23 05:35:53',
                'deleted_at' => '2020-07-23 05:35:53',
            ),
            4 => 
            array (
                'id' => 35,
                'user_type' => 2,
                'created_by' => 33,
                'name' => 'SOF',
                'signature_name' => NULL,
                'department' => 'CvilManager',
                'email' => 'sof@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$oV6QYmcvIaPG3zxP1qiMUeeQ587HBM0HJLTMr1cxbTNPcsZcr7F7e',
                'remember_token' => NULL,
                'created_at' => '2020-07-21 18:00:07',
                'updated_at' => '2020-07-23 05:34:53',
                'deleted_at' => '2020-07-23 05:34:53',
            ),
            5 => 
            array (
                'id' => 36,
                'user_type' => 1,
                'created_by' => 31,
                'name' => 'GMG',
                'signature_name' => NULL,
                'department' => 'MedDept',
                'email' => 'gmg@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$TuEmuf5QjYugcJQ7C6w0C.yUjMzRiNRyLNr7ygnAhbuJIs7277QQm',
                'remember_token' => NULL,
                'created_at' => '2020-07-21 18:04:20',
                'updated_at' => '2020-07-23 05:36:41',
                'deleted_at' => '2020-07-23 05:36:41',
            ),
            6 => 
            array (
                'id' => 37,
                'user_type' => 1,
                'created_by' => 36,
                'name' => 'GOF',
                'signature_name' => NULL,
                'department' => 'MedDept',
                'email' => 'gof@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$EWgvCQ7wWpxxBGNVDt4U8.AvJStU1UlBPN3RLmSFpchAu1T/SNDNO',
                'remember_token' => NULL,
                'created_at' => '2020-07-21 18:06:47',
                'updated_at' => '2020-07-23 05:34:59',
                'deleted_at' => '2020-07-23 05:34:59',
            ),
            7 => 
            array (
                'id' => 38,
                'user_type' => 1,
                'created_by' => 1,
                'name' => 'GHOD09',
                'signature_name' => NULL,
                'department' => 'CvilManager',
                'email' => 'ghod09@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$o1DUGjK/YDRIjSFIN/EBiu6sEV7IMFaEC7zDymi3p54PHS7NRMKuO',
                'remember_token' => NULL,
                'created_at' => '2020-07-22 02:19:02',
                'updated_at' => '2020-07-23 05:34:42',
                'deleted_at' => '2020-07-23 05:34:42',
            ),
            8 => 
            array (
                'id' => 39,
                'user_type' => 1,
                'created_by' => 38,
                'name' => 'GMG09',
                'signature_name' => NULL,
                'department' => 'CvilManager',
                'email' => 'gmg09@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$bsn2/R56ksH4Ue9q/CkwGuuieag50JPMGdHsUTxQ02Y/zyUK9KkKq',
                'remember_token' => NULL,
                'created_at' => '2020-07-22 02:21:19',
                'updated_at' => '2020-07-23 05:35:23',
                'deleted_at' => '2020-07-23 05:35:23',
            ),
            9 => 
            array (
                'id' => 40,
                'user_type' => 1,
                'created_by' => 39,
                'name' => 'GOF09',
                'signature_name' => NULL,
                'department' => 'CvilManager',
                'email' => 'gof09@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$aA/T5nThWMRBOjGCbFASjulxPv1NAvgQ/RvTtSMkjkIx1VgJ5NnFu',
                'remember_token' => NULL,
                'created_at' => '2020-07-22 02:24:13',
                'updated_at' => '2020-07-23 05:34:16',
                'deleted_at' => '2020-07-23 05:34:16',
            ),
            10 => 
            array (
                'id' => 41,
                'user_type' => 1,
                'created_by' => 1,
                'name' => 'THOD',
                'signature_name' => NULL,
                'department' => 'CvilManager',
                'email' => 'thod@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$AYfi5/CRvd7EpbwNLJhsM.r6ftulAgkohabTk62P7/hKehGb.c.ja',
                'remember_token' => NULL,
                'created_at' => '2020-07-22 05:23:43',
                'updated_at' => '2020-07-23 05:33:57',
                'deleted_at' => '2020-07-23 05:33:57',
            ),
            11 => 
            array (
                'id' => 42,
                'user_type' => 1,
                'created_by' => 1,
                'name' => 'STRATEGY HOD',
                'signature_name' => 'استيباتسيب انتسيابسي',
                'department' => 'Strategy Department',
                'email' => 'hodofstrategy@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$qOi1QJwrH1a0dQhuE3Q8berx6FDhDwORloIkoBNSBL.BTPoKj5qfm',
                'remember_token' => NULL,
                'created_at' => '2020-07-23 05:45:41',
                'updated_at' => '2020-07-23 05:45:41',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 43,
                'user_type' => 1,
                'created_by' => 1,
                'name' => 'Trainee HOD',
                'signature_name' => NULL,
                'department' => 'Trainee Department',
                'email' => 'hodoftrainee@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$.WYWR5wpQejpA18At3XBdulUgMtbZcULnK1AbYKVUlmbJdULtqFAe',
                'remember_token' => NULL,
                'created_at' => '2020-07-23 05:47:46',
                'updated_at' => '2020-07-23 05:47:46',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 44,
                'user_type' => 1,
                'created_by' => 1,
                'name' => 'Principal',
                'signature_name' => NULL,
                'department' => NULL,
                'email' => 'principal@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$7CZV0TuTbuRomtCOfwqZ/.UgCovNMS92gXRNmklPFnmRqWUheoYj6',
                'remember_token' => NULL,
                'created_at' => '2020-07-23 05:54:53',
                'updated_at' => '2020-07-23 05:54:53',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 45,
                'user_type' => 1,
                'created_by' => 43,
                'name' => 'Trainee Manager',
                'signature_name' => NULL,
                'department' => 'Trainee Department',
                'email' => 'manageroftrainee@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$d74AtStPTm6TFGVQ1Uganu535P4XSYbXJtHftz74dMIE89hwsNiJW',
                'remember_token' => NULL,
                'created_at' => '2020-07-23 06:26:55',
                'updated_at' => '2020-07-23 06:26:55',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 46,
                'user_type' => 1,
                'created_by' => 45,
                'name' => 'Trainee Officer',
                'signature_name' => NULL,
                'department' => 'Trainee Department',
                'email' => 'officeroftrainee@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$Fz4z9706ojOPt4a.SGIBBOOBtpqhdKnAklCqqv9MZW0ilFJRiaCvW',
                'remember_token' => NULL,
                'created_at' => '2020-07-23 06:30:20',
                'updated_at' => '2020-07-23 06:30:20',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}