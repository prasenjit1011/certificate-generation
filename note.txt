1. Extract the archive and put it in the folder you want

2. Run cp .env.example .env file to copy example file to .env
Then edit your .env file with DB credentials and other settings.

3. Run composer install command

4. Run php artisan migrate --seed command.
Notice: seed is important, because it will create the first admin user for you.

5. Run php artisan key:generate command.

6. If you have file/photo fields, run php artisan storage:link command.

7. If you run a SaaS project, add your Stripe credentials and plans: read more here

And that's it, go to your domain and login:

Username:	admin@admin.com
Password:	password

SQL:
ALTER TABLE `employees`  ADD `from_dtd` DATE NOT NULL DEFAULT '2020-01-01'  AFTER `last_name`,  ADD `to_dtd` DATE NOT NULL DEFAULT '2020-01-01'  AFTER `from_dtd`;
ALTER TABLE `employees` DROP `from_dtd`, DROP `to_dtd`;

ALTER TABLE `employees` ADD `courseduration` VARCHAR(25) NULL DEFAULT NULL AFTER `last_name`;
ALTER TABLE `certificates`  ADD `certificate_issue_by_post` VARCHAR(255) NULL DEFAULT NULL  AFTER `template`,  ADD `certificate_issue_by_officername` VARCHAR(255) NULL DEFAULT NULL  AFTER `certificate_issue_by_post`;